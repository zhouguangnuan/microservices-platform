package com.central.db.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.central.db.properties.MybatisPlusAutoFillProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;

/**
 * 自定义填充公共字段
 *
 * @author zlt
 * @date 2018/12/11
 * <p>
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
@Slf4j
public class DateMetaObjectHandler implements MetaObjectHandler {
    private MybatisPlusAutoFillProperties autoFillProperties;

    public DateMetaObjectHandler(MybatisPlusAutoFillProperties autoFillProperties) {
        this.autoFillProperties = autoFillProperties;
    }

    /**
     * 是否开启了插入填充
     */
    @Override
    public boolean openInsertFill() {
        return autoFillProperties.getEnableInsertFill();
    }

    /**
     * 是否开启了更新填充
     */
    @Override
    public boolean openUpdateFill() {
        return autoFillProperties.getEnableUpdateFill();
    }

    /**
     * 插入填充，字段为空自动填充
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Date now = new Date();
        if (metaObject.hasSetter(autoFillProperties.getCreateTimeField()) && null == metaObject.getValue(autoFillProperties.getCreateTimeField())) {
            log.debug("自动插入 " + autoFillProperties.getCreateTimeField());
            this.setFieldValByName(autoFillProperties.getCreateTimeField(), now, metaObject);
        }
        if (metaObject.hasSetter(autoFillProperties.getUpdateTimeField()) && null == metaObject.getValue(autoFillProperties.getUpdateTimeField())) {
            log.debug("自动插入 " + autoFillProperties.getUpdateTimeField());
            this.setFieldValByName(autoFillProperties.getUpdateTimeField(), now, metaObject);
        }
        if (metaObject.hasSetter(autoFillProperties.getCreateDateField()) && null == metaObject.getValue(autoFillProperties.getCreateDateField())) {
            log.debug("自动插入 " + autoFillProperties.getCreateDateField());
            this.setFieldValByName(autoFillProperties.getCreateDateField(), now, metaObject);
        }
        if (metaObject.hasSetter(autoFillProperties.getUpdateDateField()) && null == metaObject.getValue(autoFillProperties.getUpdateDateField())) {
            log.debug("自动插入 " + autoFillProperties.getUpdateDateField());
            this.setFieldValByName(autoFillProperties.getUpdateDateField(), now, metaObject);
        }
    }

    /**
     * 更新填充
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Date now = new Date();
        if (metaObject.hasSetter(autoFillProperties.getUpdateTimeField()) && null == metaObject.getValue(autoFillProperties.getUpdateTimeField())) {
            log.debug("自动插入 " + autoFillProperties.getUpdateTimeField());
            this.setFieldValByName(autoFillProperties.getUpdateTimeField(), now, metaObject);
        }
        if (metaObject.hasSetter(autoFillProperties.getUpdateDateField()) && null == metaObject.getValue(autoFillProperties.getUpdateDateField())) {
            log.debug("自动插入 " + autoFillProperties.getUpdateDateField());
            this.setFieldValByName(autoFillProperties.getUpdateDateField(), now, metaObject);
        }
    }
}