package com.central.common.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.central.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.dozer.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author zlt
 * 用户实体绑定spring security
 */
@Getter
@Setter
public class SysUserDTO implements SocialUserDetails {
    private Set<String> permissions;

    private Long id;
    @Excel(name = "用户姓名", height = 20, width = 30, isImportField = "true_st")
    private String username;
    private String password;
    @Excel(name = "用户昵称", height = 20, width = 30, isImportField = "true_st")
    private String nickname;
    private String headImgUrl;
    @Excel(name = "手机号码", height = 20, width = 30, isImportField = "true_st")
    private String mobile;
    @Excel(name = "性别", replace = { "男_0", "女_1" }, isImportField = "true_st")
    private Integer sex;
    @Mapping("enabled")
    private Boolean enabled;
    private String type;
    private String openId;
    @Excel(name = "创建时间", format = CommonConstant.DATETIME_FORMAT, isImportField = "true_st", width = 20)
    private Date createTime;
    @Excel(name = "修改时间", format = CommonConstant.DATETIME_FORMAT, isImportField = "true_st", width = 20)
    private Date updateTime;
    private List<SysRoleDTO> roles;
    private String roleId;
    private String oldPassword;
    private String newPassword;

    /***
     * 权限重写
     */
    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new HashSet<>();
        if (!CollectionUtils.isEmpty(getRoles())) {
            getRoles().parallelStream().forEach(role -> collection.add(new SimpleGrantedAuthority(role.getCode())));
        }
        return collection;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return getEnabled();
    }

    @Override
    public String getUserId() {
        return getOpenId();
    }
}
