package com.central.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author zlt
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysMenuDTO implements Serializable {
	private Long Id;
	private Long parentId;
	private String name;
	private String css;
	private String url;
	private String path;
	private Integer sort;
	private Integer type;
	private Boolean hidden;
	private String pathMethod;

	private List<SysMenuDTO> subMenus;
	private Long roleId;
	private Set<Long> menuIds;
}
