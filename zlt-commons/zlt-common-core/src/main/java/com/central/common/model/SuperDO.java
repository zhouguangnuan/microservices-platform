package com.central.common.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体父类
 *
 * @author zlt
 */
@Setter
@Getter
public class SuperDO<T extends Model<?>> extends Model<T> {
    /** 主键ID */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    /** 删除状态（0：正常，1：删除）*/
    @TableLogic
    private Integer isDel;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
