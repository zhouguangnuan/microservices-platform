package com.central.common.enums;


/**
 * @author zlt
 * 用户类型
 */
public enum UserTypeEnum {

	/**
	 * 前端app用户
	 */
	APP,
	/**
	 * 后端管理用户
	 */
	BACKEND
}
