package com.central.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zlt
 * 角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleDTO implements Serializable {
    private Long id;
    private String code;
    private String name;
    private Long userId;
}
