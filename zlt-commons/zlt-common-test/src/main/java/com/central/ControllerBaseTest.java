package com.central;

import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

/**
 * admin控制器单元测试基类
 */
public abstract class ControllerBaseTest extends BaseTest {

    @Autowired
    protected WebApplicationContext wac;

    private MockMvc mvc;
    private HttpHeaders baseHttpHeaders;

    @Before
    public void setupMockMvc(){
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        baseHttpHeaders = new HttpHeaders();
    }

    protected void addHttpHeaders(String headerName, @Nullable String headerValue) {
        baseHttpHeaders.add(headerName, headerValue);
    }

    public MvcResult post(String url, MultiValueMap<String, String> params) throws Exception {
        return mvc.perform(
                    MockMvcRequestBuilders.post(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .headers(baseHttpHeaders)
                        .params(null == params ? new LinkedMultiValueMap() : params)
                ).andReturn();
    }

    public MvcResult post(String url, JSONObject jsonObject) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.post(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(baseHttpHeaders)
                        .content(jsonObject.toString().getBytes())
        ).andReturn();
    }

    public MvcResult delete(String url, MultiValueMap<String, String> params) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .headers(baseHttpHeaders)
                        .params(null == params ? new LinkedMultiValueMap() : params)
        ).andReturn();
    }

    public MvcResult delete(String url, JSONObject jsonObject) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(baseHttpHeaders)
                        .content(jsonObject.toString().getBytes())
        ).andReturn();
    }

    public MvcResult put(String url, MultiValueMap<String, String> params) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.put(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .headers(baseHttpHeaders)
                        .params(null == params ? new LinkedMultiValueMap() : params)
        ).andReturn();
    }

    public MvcResult put(String url, JSONObject jsonObject) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.put(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .headers(baseHttpHeaders)
                        .content(jsonObject.toString().getBytes())
        ).andReturn();
    }

    public MvcResult get(String url, MultiValueMap<String, String> params) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.get(url)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .headers(baseHttpHeaders)
                        .params(null == params ? new LinkedMultiValueMap() : params)
        ).andReturn();
    }
}
