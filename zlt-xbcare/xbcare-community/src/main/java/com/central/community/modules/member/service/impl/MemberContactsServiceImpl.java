package com.central.community.modules.member.service.impl;

import com.central.common.service.impl.SuperServiceImpl;
import com.central.community.modules.member.mapper.MemberContactsMapper;
import com.central.community.modules.member.model.MemberContactsDO;
import com.central.community.modules.member.model.MemberContactsDTO;
import com.central.community.modules.member.service.IMemberContactsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;

/**
 * <p>名称：MemberContactsServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 16:09
 */
@Slf4j
@Service
public class MemberContactsServiceImpl extends SuperServiceImpl<MemberContactsMapper, MemberContactsDO> implements IMemberContactsService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MemberContactsDTO save(@Valid MemberContactsDTO memberContactsDTO) {
        MemberContactsDO memberContactsDo = convertor.convert(memberContactsDTO, MemberContactsDO.class);
        super.save(memberContactsDo);
        return convertor.convert(memberContactsDo, MemberContactsDTO.class);
    }
}
