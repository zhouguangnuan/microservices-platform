package com.central.community.modules.member.mapper;

import com.central.community.modules.member.model.MemberContactsDO;
import com.central.db.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>名称：MemberContactsMapper.java</p>
 * <p>描述：社区会员联系人表</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 15:59
 */
@Mapper
public interface MemberContactsMapper extends SuperMapper<MemberContactsDO> {
}
