package com.central.community.modules.member.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.annotation.LoginSysUser;
import com.central.common.constant.CommonConstant;
import com.central.common.dozer.service.GeneralConvertor;
import com.central.common.exception.BusinessException;
import com.central.common.model.Result;
import com.central.common.model.SysUserDTO;
import com.central.community.modules.member.model.MemberContactsDO;
import com.central.community.modules.member.model.MemberContactsDTO;
import com.central.community.modules.member.service.IMemberContactsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * <p>名称：MemberController.java</p>
 * <p>描述：会员模块</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 16:12
 */
@Slf4j
@Validated
@RestController
@Api(tags = "会员模块")
public class MemberController {

    @Autowired
    private GeneralConvertor convertor;

    @Autowired
    private IMemberContactsService memberContactsService;

    @ApiOperation(value = "分页查询会员联系人信息")
    @GetMapping("/admin/member/contacts")
    public Result<Page<MemberContactsDTO>> pageMemberContacts(Page page) {
        page = memberContactsService.lambdaQuery()
//                .eq(MemberContactsDO::getId, 1)
                .page(page);
        return Result.succeed(convertor.convert(page, MemberContactsDTO.class));
    }

    @ApiOperation(value = "新增会员联系人信息")
    @PostMapping("/admin/member/contacts")
    public Result<MemberContactsDTO> addMemberContacts(@LoginSysUser SysUserDTO sysUser, MemberContactsDTO memberContactsDTO) {
        memberContactsDTO.setCreateBy(sysUser.getId());
        memberContactsDTO.setCreateName(sysUser.getNickname());
        memberContactsDTO.setUpdateBy(sysUser.getId());
        memberContactsDTO.setUpdateName(sysUser.getNickname());
        memberContactsDTO = memberContactsService.save(memberContactsDTO);
        return Result.succeed(memberContactsDTO);
    }

    @ApiOperation(value = "编辑会员联系人信息")
    @PutMapping("/admin/member/contacts")
    public Result<MemberContactsDTO> editMemberContacts(@LoginSysUser SysUserDTO sysUser,
                                                        @Validated(MemberContactsDTO.Edit.class) MemberContactsDTO memberContactsDTO) {
        MemberContactsDO memberContactsDo = memberContactsService.lambdaQuery()
                .eq(MemberContactsDO::getId, memberContactsDTO.getId())
                .one();
        if (null == memberContactsDo) {
            throw new BusinessException("数据不存在！");
        }
        memberContactsDo.setUpdateBy(sysUser.getId());
        memberContactsDo.setUpdateName(sysUser.getNickname());
        memberContactsDo.setName(Optional.ofNullable(memberContactsDTO.getName()).orElse(memberContactsDo.getName()));
        memberContactsDo.setMobile(Optional.ofNullable(memberContactsDTO.getMobile()).orElse(memberContactsDo.getMobile()));
        memberContactsService.saveOrUpdate(memberContactsDo);
        return Result.succeed(convertor.convert(memberContactsDo, MemberContactsDTO.class));
    }

    @ApiOperation(value = "删除会员联系人信息")
    @DeleteMapping("/admin/member/contacts")
    public Result<MemberContactsDTO> delMemberContacts(@LoginSysUser SysUserDTO sysUser, Long id) {
        MemberContactsDO memberContactsDo = memberContactsService.lambdaQuery()
                .eq(MemberContactsDO::getId, id)
                .one();
        if (null == memberContactsDo) {
            throw new BusinessException("数据不存在！");
        }
        memberContactsDo.setUpdateBy(sysUser.getId());
        memberContactsDo.setUpdateName(sysUser.getNickname());
        memberContactsDo.setIsDel(CommonConstant.STATUS_DEL);
        memberContactsService.saveOrUpdate(memberContactsDo);
        return Result.succeed();
    }
}
