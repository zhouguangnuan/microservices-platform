package com.central.community.modules.member.service;

import com.central.common.service.ISuperService;
import com.central.community.modules.member.model.MemberContactsDO;
import com.central.community.modules.member.model.MemberContactsDTO;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * <p>名称：IMemberContactsService.java</p>
 * <p>描述：会员服务</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 16:05
 */
@Validated
public interface IMemberContactsService extends ISuperService<MemberContactsDO> {

    @Validated(MemberContactsDTO.Add.class)
    MemberContactsDTO save(@Valid MemberContactsDTO memberContactsDTO);

}
