package com.central.community.modules.member.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>名称：MemberContactsDO.java</p>
 * <p>描述：社区会员联系人</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 14:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("member_contacts")
public class MemberContactsDO extends SuperDO<MemberContactsDO> {
    /** 会员id */
    private Long memberId;
    /** 姓名 */
    private String name;
    /** 手机号 */
    private String mobile;
    /** 身份证号 */
    private String certNo;
    /** 籍贯 */
    private String nativePlace;
    /** 与长者关系 */
    private String relation;
    /** 是否监护人1是0否 */
    private Integer guardian;
    /** 住址 */
    private String address;
    /** 创建人id */
    private Long createBy;
    /** 创建人姓名 */
    private String createName;
    /** 修改人id */
    private Long updateBy;
    /** 修改人姓名 */
    private String updateName;
}

