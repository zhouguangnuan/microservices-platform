package com.central.community.modules.member.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>名称：MemberContactsDTO.java</p>
 * <p>描述：社区会员联系人</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-27 14:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel("社区会员联系人")
public class MemberContactsDTO implements Serializable {
    @NotNull(message = "id不能为空", groups = {Edit.class})
    private Long id;
    @NotNull(message = "会员id不能为空", groups = {Add.class})
    @ApiModelProperty(value = "会员id")
    private Long memberId;
    @NotBlank(message = "姓名不能为空", groups = {Add.class})
    @ApiModelProperty(value = "姓名")
    private String name;
    @NotBlank(message = "手机号不能为空", groups = {Add.class})
    @ApiModelProperty(value = "手机号")
    private String mobile;
    @NotBlank(message = "身份证号不能为空", groups = {Add.class})
    @ApiModelProperty(value = "身份证号")
    private String certNo;
    @ApiModelProperty(value = "籍贯")
    private String nativePlace;
    @ApiModelProperty(value = "与长者关系")
    private String relation;
    @ApiModelProperty(value = "是否监护人1是0否")
    private Integer guardian;
    @ApiModelProperty(value = "住址")
    private String address;
    @ApiModelProperty(value = "创建人id")
    private Long createBy;
    @ApiModelProperty(value = "创建人姓名")
    private String createName;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "修改人id")
    private Long updateBy;
    @ApiModelProperty(value = "修改人姓名")
    private String updateName;
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    @ApiModelProperty(value = "删除状态（0：正常，1：删除）")
    private String isDel;


    public @interface Add {}
    public @interface Edit {}
    public @interface Del {}
}

