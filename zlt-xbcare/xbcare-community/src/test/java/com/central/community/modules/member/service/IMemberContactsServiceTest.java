package com.central.community.modules.member.service;

import com.alibaba.fastjson.JSON;
import com.central.BaseTest;
import com.central.community.modules.member.model.MemberContactsDO;
import com.central.community.modules.member.model.MemberContactsDTO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>名称：IMemberContactsServiceTest.java</p>
 * <p>描述：</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-28 17:41
 */
public class IMemberContactsServiceTest extends BaseTest {

    @Autowired
    private IMemberContactsService service;

    @Test
    public void save() throws Exception {
        MemberContactsDTO memberContactsDTO = new MemberContactsDTO();
        memberContactsDTO.setMemberId(22L);
        memberContactsDTO.setName("name22");
        memberContactsDTO.setMobile("mobile22");
        memberContactsDTO.setCertNo("certNo22");
        memberContactsDTO = service.save(memberContactsDTO);
        System.out.println(JSON.toJSONString(memberContactsDTO));
    }

    @Test
    public void delete() throws Exception {
        service.removeById(1398218996245397505L);
    }

    @Test
    public void lambdaQuery() throws Exception {
        MemberContactsDO memberContactsDO = service.lambdaQuery()
                .eq(MemberContactsDO::getId, 1398218996245397505L)
                .one();
        Assert.assertNull(memberContactsDO);
    }
}
