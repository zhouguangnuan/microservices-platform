package com.central.community.modules.member.controller.admin;

import com.central.ControllerBaseTest;
import com.central.common.constant.SecurityConstants;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * <p>名称：MemberControllerTest.java</p>
 * <p>描述：</p>
 * <pre>
 * </pre>
 *
 * @author 周光暖
 * @version 1.0.0
 * @date 2021-05-28 17:04
 */
public class MemberControllerTest extends ControllerBaseTest {

    @Before
    public void before(){
        super.addHttpHeaders(SecurityConstants.USER_ID_HEADER, "1");
        super.addHttpHeaders(SecurityConstants.USER_HEADER, "admin");
        super.addHttpHeaders(SecurityConstants.USER_NICKNAME_HEADER, "管理员");
    }

    @Test
    public void pageMemberContacts() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("current", "1");
        params.set("size", "100");
        MvcResult result = super.get("/admin/member/contacts", params);
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    public void addMemberContacts() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("memberId", "111");
        params.add("name", "name111");
        params.add("mobile", "mobile111");
        params.add("certNo", "certNo111");
        MvcResult result = super.post("/admin/member/contacts", params);
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    public void editMemberContacts() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        MvcResult result = super.put("/admin/member/contacts", params);
        System.out.println(result.getResponse().getContentAsString());
    }
}
