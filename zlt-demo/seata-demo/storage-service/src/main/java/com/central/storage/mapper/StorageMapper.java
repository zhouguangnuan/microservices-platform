package com.central.storage.mapper;

import com.central.db.mapper.SuperMapper;
import com.central.storage.model.StorageDO;

/**
 * @author zlt
 * @date 2019/9/14
 */
public interface StorageMapper extends SuperMapper<StorageDO> {

}
