package com.central.storage.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.storage.model.StorageDO;
import com.central.storage.mapper.StorageMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zlt
 * @date 2019/9/14
 */
@Slf4j
@Service
public class StorageService {
    @Resource
    private StorageMapper storageMapper;

    /**
     * 减库存
     * 
     * @param commodityCode 商品编号
     * @param count 数量
     */
    //@Transactional(rollbackFor = Exception.class)
    public void deduct(String commodityCode, int count) {
        QueryWrapper<StorageDO> wrapper = new QueryWrapper<>();
        wrapper.setEntity(new StorageDO().setCommodityCode(commodityCode));
        StorageDO storage = storageMapper.selectOne(wrapper);
        storage.setCount(storage.getCount() - count);

        storageMapper.updateById(storage);
    }
}
