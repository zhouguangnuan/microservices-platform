package com.sharding.demo.service;

import com.central.common.service.ISuperService;
import com.sharding.demo.model.UserDO;

/**
* @author zlt
 */
public interface IUserService extends ISuperService<UserDO> {

}
