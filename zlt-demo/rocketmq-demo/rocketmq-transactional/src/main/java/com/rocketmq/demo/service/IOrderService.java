package com.rocketmq.demo.service;

import com.rocketmq.demo.model.OrderDTO;

/**
* @author zlt
 */
public interface IOrderService {
    void save(OrderDTO order);
}
