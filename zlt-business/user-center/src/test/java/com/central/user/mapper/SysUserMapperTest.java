package com.central.user.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.user.model.SysUserDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * SysUserMapperTest单元测试用例
 *
 * @author zlt
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SysUserMapperTest {
	@Resource
	private SysUserMapper sysUserMapper;

	@Test
	public void testFindByUsername() {
		List<SysUserDO> users = sysUserMapper.selectList(
				new QueryWrapper<SysUserDO>().eq("username", "admin")
		);
		assertThat(users.size()).isGreaterThan(0);
	}
}
