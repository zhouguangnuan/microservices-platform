package com.central.user.service;

import com.alibaba.fastjson.JSON;
import com.central.common.dozer.service.GeneralConvertor;
import com.central.common.model.SysUserDTO;
import static org.assertj.core.api.Assertions.*;

import com.central.common.model.PageResult;
import com.central.user.model.SysUserDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * SysUserServiceTest单元测试用例
 *
 * @author zlt
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SysUserServiceTest {
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	protected GeneralConvertor convertor;

	@Test
	public void testFindByUsername() {
		SysUserDTO loginAppUser = sysUserService.findByUsername("admin");
		assertThat(loginAppUser).isNotNull();
	}

	@Test
	public void testFindUsers() {
		Map<String, Object> params = new HashMap<>(2);
		params.put("page", 1);
		params.put("limit", 10);
		PageResult<SysUserDO> page = sysUserService.findUsers(params);
		assertThat(page).isNotNull();
		assertThat(page.getCount()).isGreaterThan(0);
	}

	@Test
	public void getSysUser() throws Exception {
		SysUserDO sysUserDO = sysUserService.lambdaQuery().eq(SysUserDO::getUsername, "admin").one();
		SysUserDTO sysUserDTO = convertor.convert(sysUserDO, SysUserDTO.class);
		System.out.println(JSON.toJSONString(sysUserDO));
		System.out.println(JSON.toJSONString(sysUserDTO));
	}
}
