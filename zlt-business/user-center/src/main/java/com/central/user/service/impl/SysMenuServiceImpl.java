package com.central.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.central.common.model.SysMenuDTO;
import com.central.common.service.impl.SuperServiceImpl;
import com.central.user.model.SysMenuDO;
import com.central.user.model.SysRoleMenuDO;
import com.central.user.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.central.user.mapper.SysMenuMapper;
import com.central.user.service.ISysMenuService;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;

/**
 * @author 作者 owen E-mail: 624191343@qq.com
 */
@Slf4j
@Service
public class SysMenuServiceImpl extends SuperServiceImpl<SysMenuMapper, SysMenuDO> implements ISysMenuService {
 	@Resource
	private ISysRoleMenuService roleMenuService;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void setMenuToRole(Long roleId, Set<Long> menuIds) {
		roleMenuService.delete(roleId, null);

		if (!CollectionUtils.isEmpty(menuIds)) {
			List<SysRoleMenuDO> roleMenus = new ArrayList<>(menuIds.size());
			menuIds.forEach(menuId -> roleMenus.add(new SysRoleMenuDO(roleId, menuId)));
			roleMenuService.saveBatch(roleMenus);
		}
	}

	/**
	 * 角色菜单列表
	 * @param roleIds
	 * @return
	 */
	@Override
	public List<SysMenuDTO> findByRoles(Set<Long> roleIds) {
		return convertor.convert(roleMenuService.findMenusByRoleIds(roleIds, null), SysMenuDTO.class);
	}

	/**
	 * 角色菜单列表
	 * @param roleIds 角色ids
	 * @param roleIds 是否菜单
	 * @return
	 */
	@Override
	public List<SysMenuDTO> findByRoles(Set<Long> roleIds, Integer type) {
		return convertor.convert(roleMenuService.findMenusByRoleIds(roleIds, type), SysMenuDTO.class);
	}

	@Override
	public List<SysMenuDTO> findByRoleCodes(Set<String> roleCodes, Integer type) {
		return convertor.convert(roleMenuService.findMenusByRoleCodes(roleCodes, type), SysMenuDTO.class);
	}

    /**
     * 查询所有菜单
     */
	@Override
	public List<SysMenuDTO> findAll() {
		return convertor.convert(baseMapper.selectList(
                new QueryWrapper<SysMenuDO>().orderByAsc("sort")
        ), SysMenuDTO.class);
	}

    /**
     * 查询所有一级菜单
     */
	@Override
	public List<SysMenuDTO> findOnes() {
        return convertor.convert(baseMapper.selectList(
                new QueryWrapper<SysMenuDO>()
                        .eq("type", 1)
                        .orderByAsc("sort")
        ), SysMenuDTO.class);
	}
}
