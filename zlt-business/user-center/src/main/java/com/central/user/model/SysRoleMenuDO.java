package com.central.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;

/**
 * @author zlt
 * @date 2019/7/30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_menu")
public class SysRoleMenuDO extends Model<SysRoleMenuDO> {
	private Long roleId;
	private Long menuId;
}
