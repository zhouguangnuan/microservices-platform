package com.central.user.service;

import com.central.common.model.PageResult;
import com.central.common.model.Result;
import com.central.common.model.SysRoleDTO;
import com.central.common.service.ISuperService;
import com.central.user.model.SysRoleDO;

import java.util.List;
import java.util.Map;

/**
 * @author zlt
 * <p>
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
public interface ISysRoleService extends ISuperService<SysRoleDO> {
	void saveRole(SysRoleDTO sysRole) throws Exception;

	void deleteRole(Long id);

	/**
	 * 角色列表
	 * @param params
	 * @return
	 */
	PageResult<SysRoleDO> findRoles(Map<String, Object> params);

	/**
	 * 新增或更新角色
	 * @param sysRole
	 * @return Result
	 */
	Result saveOrUpdateRole(SysRoleDTO sysRole) throws Exception;

	/**
	 * 查询所有角色
	 * @return
	 */
	List<SysRoleDTO> findAll();
}
