package com.central.user.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.central.common.model.SuperDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zlt
 * 角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role")
public class SysRoleDO extends SuperDO {
    private static final long serialVersionUID = 4497149010220586111L;
    private String code;
    private String name;

    @TableField(exist = false)
    private Long userId;
}
