package com.central.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zlt
 * @date 2019/7/30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_user")
public class SysRoleUserDO extends Model<SysRoleUserDO> {
	private Long userId;
	private Long roleId;
}
