package com.central.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.central.common.lock.DistributedLock;
import com.central.common.model.PageResult;
import com.central.common.model.Result;
import com.central.common.model.SysRoleDTO;
import com.central.common.service.impl.SuperServiceImpl;
import com.central.user.mapper.SysRoleMapper;
import com.central.user.mapper.SysRoleMenuMapper;
import com.central.user.mapper.SysUserRoleMapper;
import com.central.user.model.SysRoleDO;
import com.central.user.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author 作者 owen E-mail: 624191343@qq.com
 */
@Slf4j
@Service
public class SysRoleServiceImpl extends SuperServiceImpl<SysRoleMapper, SysRoleDO> implements ISysRoleService {
    private final static String LOCK_KEY_ROLECODE = "rolecode:";

    @Resource
    private SysUserRoleMapper userRoleMapper;

    @Resource
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private DistributedLock lock;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveRole(SysRoleDTO sysRole) throws Exception {
        String roleCode = sysRole.getCode();
        super.saveIdempotency(convertor.convert(sysRole, SysRoleDO.class), lock
                , LOCK_KEY_ROLECODE+roleCode, new QueryWrapper<SysRoleDO>().eq("code", roleCode), "角色code已存在");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteRole(Long id) {
        baseMapper.deleteById(id);
        roleMenuMapper.delete(id, null);
        userRoleMapper.deleteUserRole(null, id);
    }

    @Override
    public PageResult<SysRoleDO> findRoles(Map<String, Object> params) {
        Integer curPage = MapUtils.getInteger(params, "page");
        Integer limit = MapUtils.getInteger(params, "limit");
        Page<SysRoleDO> page = new Page<>(curPage == null ? 0 : curPage, limit == null ? -1 : limit);
        List<SysRoleDO> list = baseMapper.findList(page, params);
        return PageResult.<SysRoleDO>builder().data(list).code(0).count(page.getTotal()).build();
    }

    @Override
    @Transactional
    public Result saveOrUpdateRole(SysRoleDTO sysRole) throws Exception {
        if (sysRole.getId() == null) {
            this.saveRole(sysRole);
        } else {
            baseMapper.updateById(convertor.convert(sysRole, SysRoleDO.class));
        }
        return Result.succeed("操作成功");
    }

    @Override
    public List<SysRoleDTO> findAll() {
        return convertor.convert(baseMapper.findAll(), SysRoleDTO.class);
    }
}
