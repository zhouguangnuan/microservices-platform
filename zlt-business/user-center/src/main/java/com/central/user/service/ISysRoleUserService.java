package com.central.user.service;

import com.central.common.model.SysRoleDTO;
import com.central.common.service.ISuperService;
import com.central.user.model.SysRoleDO;
import com.central.user.model.SysRoleUserDO;

import java.util.List;

/**
 * @author zlt
 */
public interface ISysRoleUserService extends ISuperService<SysRoleUserDO> {
	int deleteUserRole(Long userId, Long roleId);

	int saveUserRoles(Long userId, Long roleId);

	/**
	 * 根据用户id获取角色
	 *
	 * @param userId
	 * @return
	 */
	List<SysRoleDTO> findRolesByUserId(Long userId);

	/**
	 * 根据用户ids 获取
	 *
	 * @param userIds
	 * @return
	 */
	List<SysRoleDTO> findRolesByUserIds(List<Long> userIds);
}
