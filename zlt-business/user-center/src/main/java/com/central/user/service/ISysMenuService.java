package com.central.user.service;

import com.central.common.model.SysMenuDTO;
import com.central.common.service.ISuperService;
import com.central.user.model.SysMenuDO;

import java.util.List;
import java.util.Set;

/**
 * @author zlt
 */
public interface ISysMenuService extends ISuperService<SysMenuDO> {
	/**
	 * 查询所有菜单
	 */
	List<SysMenuDTO> findAll();

	/**
	 * 查询所有一级菜单
	 */
	List<SysMenuDTO> findOnes();

	/**
	 * 角色分配菜单
	 * @param roleId
	 * @param menuIds
	 */
	void setMenuToRole(Long roleId, Set<Long> menuIds);

	/**
	 * 角色菜单列表
	 * @param roleIds 角色ids
	 * @return
	 */
	List<SysMenuDTO> findByRoles(Set<Long> roleIds);

	/**
	 * 角色菜单列表
	 * @param roleIds 角色ids
	 * @param roleIds 是否菜单
	 * @return
	 */
	List<SysMenuDTO> findByRoles(Set<Long> roleIds, Integer type);

	/**
	 * 角色菜单列表
	 * @param roleCodes
	 * @return
	 */
	List<SysMenuDTO> findByRoleCodes(Set<String> roleCodes, Integer type);
}
