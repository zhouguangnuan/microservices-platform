package com.central.user.service;

import com.central.common.model.PageResult;
import com.central.common.model.Result;
import com.central.common.model.SysRoleDTO;
import com.central.common.model.SysUserDTO;
import com.central.common.service.ISuperService;
import com.central.user.model.SysUserDO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author zlt
 * <p>
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
public interface ISysUserService extends ISuperService<SysUserDO> {
	/**
	 * 获取UserDetails对象
	 * @param username
	 * @return
	 */
	SysUserDTO findByUsername(String username);

	SysUserDTO findByOpenId(String username);

	SysUserDTO findByMobile(String username);

	/**
	 * 通过SysUser 转换为 SysUserDTO，把roles和permissions也查询出来
	 * @param sysUser
	 * @return
	 */
	SysUserDTO getSysUser(SysUserDTO sysUser);

	/**
	 * 根据用户名查询用户
	 * @param username
	 * @return
	 */
	SysUserDTO selectByUsername(String username);
	/**
	 * 根据手机号查询用户
	 * @param mobile
	 * @return
	 */
	SysUserDTO selectByMobile(String mobile);
	/**
	 * 根据openId查询用户
	 * @param openId
	 * @return
	 */
	SysUserDTO selectByOpenId(String openId);

	/**
	 * 用户分配角色
	 * @param id
	 * @param roleIds
	 */
	void setRoleToUser(Long id, Set<Long> roleIds);

	/**
	 * 更新密码
	 * @param id
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	Result updatePassword(Long id, String oldPassword, String newPassword);

	/**
	 * 用户列表
	 * @param params
	 * @return
	 */
	PageResult<SysUserDO> findUsers(Map<String, Object> params);


	/**
	 * 用户角色列表
	 * @param userId
	 * @return
	 */
	List<SysRoleDTO> findRolesByUserId(Long userId);

	/**
	 * 状态变更
	 * @param params
	 * @return
	 */
	Result updateEnabled(Map<String, Object> params);

	/**
	 * 查询全部用户
	 * @param params
	 * @return
	 */
	List<SysUserDTO> findAllUsers(Map<String, Object> params);

	Result saveOrUpdateUser(SysUserDO sysUser) throws Exception;

	/**
	 * 删除用户
	 */
	boolean delUser(Long id);
}
