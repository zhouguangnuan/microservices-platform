package com.central.user.service;

import com.central.common.service.ISuperService;
import com.central.user.model.SysMenuDO;
import com.central.user.model.SysRoleMenuDO;

import java.util.List;
import java.util.Set;

/**
 * @author zlt
 */
public interface ISysRoleMenuService extends ISuperService<SysRoleMenuDO> {
	int save(Long roleId, Long menuId);

	int delete(Long roleId, Long menuId);

	List<SysMenuDO> findMenusByRoleIds(Set<Long> roleIds, Integer type);

	List<SysMenuDO> findMenusByRoleCodes(Set<String> roleCodes, Integer type);
}
