package com.central.user.service.impl;

import com.central.common.model.SysRoleDTO;
import com.central.common.service.impl.SuperServiceImpl;
import com.central.user.mapper.SysUserRoleMapper;
import com.central.user.model.SysRoleUserDO;
import com.central.user.service.ISysRoleUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zlt
 */
@Slf4j
@Service
public class SysRoleUserServiceImpl extends SuperServiceImpl<SysUserRoleMapper, SysRoleUserDO> implements ISysRoleUserService {
 	@Resource
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public int deleteUserRole(Long userId, Long roleId) {
		return sysUserRoleMapper.deleteUserRole(userId, roleId);
	}

	@Override
	public int saveUserRoles(Long userId, Long roleId) {
		return sysUserRoleMapper.saveUserRoles(userId, roleId);
	}

	@Override
	public List<SysRoleDTO> findRolesByUserId(Long userId) {
		return convertor.convert(sysUserRoleMapper.findRolesByUserId(userId), SysRoleDTO.class);
	}

	@Override
	public List<SysRoleDTO> findRolesByUserIds(List<Long> userIds) {
		return convertor.convert(sysUserRoleMapper.findRolesByUserIds(userIds), SysRoleDTO.class);
	}
}
