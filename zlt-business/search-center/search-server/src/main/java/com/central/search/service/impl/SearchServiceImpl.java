package com.central.search.service.impl;

import com.central.common.model.PageResult;
import com.central.es.utils.SearchBuilder;
import com.central.search.model.SearchDTO;
import com.central.search.service.ISearchService;
import com.fasterxml.jackson.databind.JsonNode;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 通用搜索
 *
 * @author zlt
 * @date 2019/4/24
 */
@Service
public class SearchServiceImpl implements ISearchService {
    private final RestHighLevelClient client;

    public SearchServiceImpl(RestHighLevelClient client) {
        this.client = client;
    }

    /**
     * StringQuery通用搜索
     * @param indexName 索引名
     * @param searchDTO 搜索
     * @return
     */
    @Override
    public PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO) throws IOException {
        return SearchBuilder.builder(client, indexName)
                .setStringQuery(searchDTO.getQueryStr())
                .addSort(searchDTO.getSortCol(), SortOrder.DESC)
                .setIsHighlight(searchDTO.getIsHighlighter())
                .getPage(searchDTO.getPage(), searchDTO.getLimit());
    }
}
