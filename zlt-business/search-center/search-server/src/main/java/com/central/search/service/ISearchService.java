package com.central.search.service;

import com.central.common.model.PageResult;
import com.central.search.model.SearchDTO;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * @author zlt
 * @date 2019/4/24
 */
public interface ISearchService {
    /**
     * StringQuery通用搜索
     * @param indexName 索引名
     * @param searchDTO 搜索
     * @return
     */
    PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO) throws IOException;
}
