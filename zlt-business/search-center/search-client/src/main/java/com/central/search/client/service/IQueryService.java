package com.central.search.client.service;

import com.central.common.model.PageResult;
import com.central.search.model.LogicDelDTO;
import com.central.search.model.SearchDTO;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;

/**
 * 搜索客户端接口
 *
 * @author zlt
 * @date 2019/4/24
 */
public interface IQueryService {
    /**
     * 查询文档列表
     * @param indexName 索引名
     * @param searchDTO 搜索
     */
    PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO);

    /**
     * 查询文档列表
     * @param indexName 索引名
     * @param searchDTO 搜索
     * @param logicDelDTO 逻辑删除
     */
    PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO, LogicDelDTO logicDelDTO);

    /**
     * 访问统计聚合查询
     * @param indexName 索引名
     * @param routing es的路由
     */
    Map<String, Object> requestStatAgg(String indexName, String routing);
}
