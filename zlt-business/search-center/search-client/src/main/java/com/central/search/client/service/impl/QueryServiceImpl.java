package com.central.search.client.service.impl;

import cn.hutool.core.util.StrUtil;
import com.central.common.model.PageResult;
import com.central.search.client.feign.AggregationService;
import com.central.search.client.feign.SearchService;
import com.central.search.client.service.IQueryService;
import com.central.search.model.LogicDelDTO;
import com.central.search.model.SearchDTO;
import com.fasterxml.jackson.databind.JsonNode;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 搜索客户端Service
 *
 * @author zlt
 * @date 2019/4/24
 */
public class QueryServiceImpl implements IQueryService {
    @Resource
    private SearchService searchService;

    @Resource
    private AggregationService aggregationService;

    @Override
    public PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO) {
        return strQuery(indexName, searchDTO, null);
    }

    @Override
    public PageResult<JsonNode> strQuery(String indexName, SearchDTO searchDTO, LogicDelDTO logicDelDTO) {
        setLogicDelQueryStr(searchDTO, logicDelDTO);
        return searchService.strQuery(indexName, searchDTO);
    }

    /**
     * 拼装逻辑删除的条件
     * @param searchDTO 搜索
     * @param logicDelDTO 逻辑删除
     */
    private void setLogicDelQueryStr(SearchDTO searchDTO, LogicDelDTO logicDelDTO) {
        if (logicDelDTO != null
                && StrUtil.isNotEmpty(logicDelDTO.getLogicDelField())
                && StrUtil.isNotEmpty(logicDelDTO.getLogicNotDelValue())) {
            String result;
            //搜索条件
            String queryStr = searchDTO.getQueryStr();
            //拼凑逻辑删除的条件
            String logicStr = logicDelDTO.getLogicDelField() + ":" + logicDelDTO.getLogicNotDelValue();
            if (StrUtil.isNotEmpty(queryStr)) {
                result = "(" + queryStr + ") AND " + logicStr;
            } else {
                result = logicStr;
            }
            searchDTO.setQueryStr(result);
        }
    }

    /**
     * 访问统计聚合查询
     * @param indexName 索引名
     * @param routing es的路由
     */
    @Override
    public Map<String, Object> requestStatAgg(String indexName, String routing) {
        return aggregationService.requestStatAgg(indexName, routing);
    }
}
